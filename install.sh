#!/usr/bin/env bash

echo "Executing docker-compose up"

sudo docker-compose up -d --build

echo "Generating database tables"

sudo docker container exec -d restaurantfinder_web_1 bin/console doctrine:schema:update -f

echo "Go to http://localhost:81/register and create user this user will be first admin with permissions to create other admins"
read -p "Press enter to continue if you are registered or Ctrl + C to exit"

sudo chmod 775 ./create-admin.sh

./create-admin.sh