<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190502125546 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TEMPORARY TABLE __temp__restaurant AS SELECT id, name, description, image FROM restaurant');
        $this->addSql('DROP TABLE restaurant');
        $this->addSql('CREATE TABLE restaurant (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name VARCHAR(255) NOT NULL COLLATE BINARY, image VARCHAR(255) NOT NULL COLLATE BINARY, description CLOB DEFAULT NULL, keywords CLOB NOT NULL)');
        $this->addSql('INSERT INTO restaurant (id, name, description, image) SELECT id, name, description, image FROM __temp__restaurant');
        $this->addSql('DROP TABLE __temp__restaurant');
        $this->addSql('DROP INDEX IDX_DA5EC7CB30609C2E');
        $this->addSql('DROP INDEX IDX_DA5EC7CB35592D86');
        $this->addSql('DROP INDEX IDX_DA5EC7CB9D86650F');
        $this->addSql('CREATE TEMPORARY TABLE __temp__xsession_rank AS SELECT id, user_id_id, restaurant_id_id, x_session_id, rank FROM xsession_rank');
        $this->addSql('DROP TABLE xsession_rank');
        $this->addSql('CREATE TABLE xsession_rank (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, user_id_id INTEGER DEFAULT NULL, restaurant_id_id INTEGER DEFAULT NULL, x_session_id INTEGER DEFAULT NULL, rank INTEGER NOT NULL, CONSTRAINT FK_DA5EC7CB9D86650F FOREIGN KEY (user_id_id) REFERENCES user (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_DA5EC7CB35592D86 FOREIGN KEY (restaurant_id_id) REFERENCES restaurant (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_DA5EC7CB30609C2E FOREIGN KEY (x_session_id) REFERENCES xsession (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO xsession_rank (id, user_id_id, restaurant_id_id, x_session_id, rank) SELECT id, user_id_id, restaurant_id_id, x_session_id, rank FROM __temp__xsession_rank');
        $this->addSql('DROP TABLE __temp__xsession_rank');
        $this->addSql('CREATE INDEX IDX_DA5EC7CB30609C2E ON xsession_rank (x_session_id)');
        $this->addSql('CREATE INDEX IDX_DA5EC7CB35592D86 ON xsession_rank (restaurant_id_id)');
        $this->addSql('CREATE INDEX IDX_DA5EC7CB9D86650F ON xsession_rank (user_id_id)');
        $this->addSql('DROP INDEX IDX_57BF5FE6E5784294');
        $this->addSql('CREATE TEMPORARY TABLE __temp__xsession AS SELECT id, creator_user_id_id, description FROM xsession');
        $this->addSql('DROP TABLE xsession');
        $this->addSql('CREATE TABLE xsession (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, creator_user_id_id INTEGER DEFAULT NULL, description CLOB NOT NULL COLLATE BINARY, session_id VARCHAR(255) NOT NULL, CONSTRAINT FK_57BF5FE6E5784294 FOREIGN KEY (creator_user_id_id) REFERENCES user (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO xsession (id, creator_user_id_id, description) SELECT id, creator_user_id_id, description FROM __temp__xsession');
        $this->addSql('DROP TABLE __temp__xsession');
        $this->addSql('CREATE INDEX IDX_57BF5FE6E5784294 ON xsession (creator_user_id_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TEMPORARY TABLE __temp__restaurant AS SELECT id, name, description, image FROM restaurant');
        $this->addSql('DROP TABLE restaurant');
        $this->addSql('CREATE TABLE restaurant (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name VARCHAR(255) NOT NULL, image VARCHAR(255) NOT NULL, description CLOB NOT NULL COLLATE BINARY)');
        $this->addSql('INSERT INTO restaurant (id, name, description, image) SELECT id, name, description, image FROM __temp__restaurant');
        $this->addSql('DROP TABLE __temp__restaurant');
        $this->addSql('DROP INDEX IDX_57BF5FE6E5784294');
        $this->addSql('CREATE TEMPORARY TABLE __temp__xsession AS SELECT id, creator_user_id_id, description FROM xsession');
        $this->addSql('DROP TABLE xsession');
        $this->addSql('CREATE TABLE xsession (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, creator_user_id_id INTEGER DEFAULT NULL, description CLOB NOT NULL)');
        $this->addSql('INSERT INTO xsession (id, creator_user_id_id, description) SELECT id, creator_user_id_id, description FROM __temp__xsession');
        $this->addSql('DROP TABLE __temp__xsession');
        $this->addSql('CREATE INDEX IDX_57BF5FE6E5784294 ON xsession (creator_user_id_id)');
        $this->addSql('DROP INDEX IDX_DA5EC7CB9D86650F');
        $this->addSql('DROP INDEX IDX_DA5EC7CB35592D86');
        $this->addSql('DROP INDEX IDX_DA5EC7CB30609C2E');
        $this->addSql('CREATE TEMPORARY TABLE __temp__xsession_rank AS SELECT id, user_id_id, restaurant_id_id, x_session_id, rank FROM xsession_rank');
        $this->addSql('DROP TABLE xsession_rank');
        $this->addSql('CREATE TABLE xsession_rank (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, user_id_id INTEGER DEFAULT NULL, restaurant_id_id INTEGER DEFAULT NULL, x_session_id INTEGER DEFAULT NULL, rank INTEGER NOT NULL)');
        $this->addSql('INSERT INTO xsession_rank (id, user_id_id, restaurant_id_id, x_session_id, rank) SELECT id, user_id_id, restaurant_id_id, x_session_id, rank FROM __temp__xsession_rank');
        $this->addSql('DROP TABLE __temp__xsession_rank');
        $this->addSql('CREATE INDEX IDX_DA5EC7CB9D86650F ON xsession_rank (user_id_id)');
        $this->addSql('CREATE INDEX IDX_DA5EC7CB35592D86 ON xsession_rank (restaurant_id_id)');
        $this->addSql('CREATE INDEX IDX_DA5EC7CB30609C2E ON xsession_rank (x_session_id)');
    }
}
