<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190426080749 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE restaurant (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name VARCHAR(255) NOT NULL, description CLOB NOT NULL, image VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE TABLE xsession_rank (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, user_id_id INTEGER DEFAULT NULL, restaurant_id_id INTEGER DEFAULT NULL, x_session_id INTEGER DEFAULT NULL, rank INTEGER NOT NULL)');
        $this->addSql('CREATE INDEX IDX_DA5EC7CB9D86650F ON xsession_rank (user_id_id)');
        $this->addSql('CREATE INDEX IDX_DA5EC7CB35592D86 ON xsession_rank (restaurant_id_id)');
        $this->addSql('CREATE INDEX IDX_DA5EC7CB30609C2E ON xsession_rank (x_session_id)');
        $this->addSql('CREATE TABLE xsession (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, creator_user_id_id INTEGER DEFAULT NULL, description CLOB NOT NULL)');
        $this->addSql('CREATE INDEX IDX_57BF5FE6E5784294 ON xsession (creator_user_id_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE restaurant');
        $this->addSql('DROP TABLE xsession_rank');
        $this->addSql('DROP TABLE xsession');
    }
}
