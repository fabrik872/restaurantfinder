<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/user")
 */
class UserController extends AbstractController
{
    private $columns;

    public function __construct()
    {
        $this->columns = ["id", "email", "roles", "createDate"];
    }

    /**
     * @Route("/", name="user_index", methods={"GET"})
     */
    public function index(UserRepository $userRepository): Response
    {
        return $this->render('user/index.html.twig', [
            'columns' => $this->columns,
            'users' => $userRepository->findAll(),
        ]);
    }

    /**
     * @Route("/datatables", name="user_index_ajax", methods={"POST"})
     */
    public function indexAjax(UserRepository $userRepository, Request $request): JsonResponse
    {
        $column = $this->columns[$request->request->get('order')[0]['column']];

        $userRepository->select(
            $request->request->get('start'),
            $request->request->get('length'),
            $column,
            $request->request->get('order')[0]['dir'],
            $this->columns,
            $request->request->get('search')['value']
        );

        $data = $userRepository->arrayResults();

        foreach ($data as $key => $row) {
            $data[$key]["actions"]["show"] = $this->generateUrl("user_show", ["id"=>$row["id"]]);
            $data[$key]["actions"]["edit"] = $this->generateUrl("user_edit", ["id"=>$row["id"]]);
            $data[$key]["createDate"] = $row['createDate']->format("d. M Y H:i:s");
        }

        $json = new \stdClass();
        $json->draw = $request->request->get('draw');
        $json->recordsTotal = $userRepository->findTotalUsers();
        $json->recordsFiltered = $userRepository->totalFiltered();
        $json->data = $data;

        return new JsonResponse($json, 200);
    }

    /**
     * @Route("/new", name="user_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_show", methods={"GET"})
     */
    public function show(User $user): Response
    {
        return $this->render('user/show.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="user_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, User $user): Response
    {
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
dump($form);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_index', [
                'id' => $user->getId(),
            ]);
        }

        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_delete", methods={"DELETE"})
     */
    public function delete(Request $request, User $user): Response
    {
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();
        }

        return $this->redirectToRoute('user_index');
    }
}
