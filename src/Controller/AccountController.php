<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AccountController
 * @package App\Controller
 * @Route("/account")
 */
class AccountController extends AbstractController
{
    /**
     * @Route("/", name="account")
     */
    public function index()
    {
        return $this->render('account/index.html.twig', [
        ]);
    }

    /**
     * @Route("/change_pw", name="changePw")
     */
    public function changePw(Request $request)
    {
        /**
         * @var $user User
         */

        if ($this->isCsrfTokenValid('change_pw', $request->get('csfr_token'))) {
            $em = $this->getDoctrine()->getManager();
            $user = $this->getUser();

            if (password_verify($request->get('oldPassword'), $user->getPassword())) {
                $user->setPassword(password_hash($request->get('newPassword'), PASSWORD_DEFAULT));
                $em->persist($user);
                $em->flush();

                $this->addFlash('success', 'Password succesfully changed');
            } else{
                $this->addFlash('danger', 'Old password not match');
            }
        }
        return $this->redirectToRoute('account');
    }
}
