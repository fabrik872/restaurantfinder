<?php

namespace App\Controller;

use App\Entity\Restaurant;
use App\Entity\User;
use App\Entity\XSession;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();

        return $this->render('default/index.html.twig', [
            'totalUsers' => $em->getRepository(User::class)->findTotalUsers(),
            'totalRestaurants' => $em->getRepository(Restaurant::class)->findTotalRestaurants(),
            'dailySessions' => $em->getRepository(XSession::class)->dailyRestaurants(),
        ]);
    }
}
