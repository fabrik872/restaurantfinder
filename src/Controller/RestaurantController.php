<?php

namespace App\Controller;

use App\Entity\Restaurant;
use App\Form\RestaurantType;
use App\Repository\RestaurantRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/restaurant")
 */
class RestaurantController extends AbstractController
{
    private $columns;

    public function __construct()
    {
        $this->columns = ["id", "name", "description"];
    }

    /**
     * @Route("/", name="restaurant_index", methods={"GET"})
     */
    public function index(RestaurantRepository $restaurantRepository): Response
    {
        return $this->render('restaurant/index.html.twig', [
            'columns' => $this->columns,
            'restaurants' => $restaurantRepository->findAll(),
        ]);
    }

    /**
     * @Route("/datatables", name="restaurant_index_ajax", methods={"POST"})
     */
    public function indexAjax(RestaurantRepository $restaurantRepository, Request $request): JsonResponse
    {
        $column = $this->columns[$request->request->get('order')[0]['column']];

        $restaurantRepository->select(
            $request->request->get('start'),
            $request->request->get('length'),
            $column,
            $request->request->get('order')[0]['dir'],
            $this->columns,
            $request->request->get('search')['value']
        );

        $data = $restaurantRepository->arrayResults();

        foreach ($data as $key => $row) {
            $data[$key]["actions"]["show"] = $this->generateUrl("restaurant_show", ["id"=>$row["id"]]);
            $data[$key]["actions"]["edit"] = $this->generateUrl("restaurant_edit", ["id"=>$row["id"]]);
        }

        $json = new \stdClass();
        $json->draw = $request->request->get('draw');
        $json->recordsTotal = $restaurantRepository->findTotalRestaurants();
        $json->recordsFiltered = $restaurantRepository->totalFiltered();
        $json->data = $data;

        return new JsonResponse($json, 200);
    }

    /**
     * @Route("/new", name="restaurant_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $restaurant = new Restaurant();
        $form = $this->createForm(RestaurantType::class, $restaurant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($restaurant);

            $entityManager->flush();

            return $this->redirectToRoute('restaurant_index');
        }

        return $this->render('restaurant/new.html.twig', [
            'restaurant' => $restaurant,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="restaurant_show", methods={"GET"})
     */
    public function show(Restaurant $restaurant): Response
    {
        return $this->render('restaurant/show.html.twig', [
            'restaurant' => $restaurant,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="restaurant_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Restaurant $restaurant): Response
    {
        $form = $this->createForm(RestaurantType::class, $restaurant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('restaurant_index', [
                'id' => $restaurant->getId(),
            ]);
        }

        return $this->render('restaurant/edit.html.twig', [
            'restaurant' => $restaurant,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="restaurant_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Restaurant $restaurant): Response
    {
        if ($this->isCsrfTokenValid('delete' . $restaurant->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($restaurant);
            $entityManager->flush();
        }

        return $this->redirectToRoute('restaurant_index');
    }
}
