<?php

namespace App\Controller;

use App\Entity\Restaurant;
use App\Entity\User;
use App\Entity\UserList;
use App\Entity\XSession;
use App\Entity\XSessionRank;
use App\Form\XSessionType;
use App\Repository\XSessionRepository;
use Gregwar\Image\Image;
use phpDocumentor\Reflection\Types\This;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;


/**
 * Class SessionController
 *
 * @IsGranted("ROLE_USER")
 * @Route("/session")
 */
class SessionController extends AbstractController
{
    private $columns;
    private $tokenManager;

    public function __construct(CsrfTokenManagerInterface $tokenManager)
    {
        $this->columns = ["id", "host", "finalRestaurant", "description", "dateCreated"];
        $this->tokenManager = $tokenManager;
    }

    /**
     * @Route("/", name="session")
     */
    public function index(): Response
    {
        $sessions = $this->getDoctrine()->getRepository(XSession::class)->findSessionWithUser($this->getUser());
        foreach ($sessions as $key => $session) {
            if ($this->getDoctrine()->getRepository(UserList::class)->isEveryoneFinished($session)->getIsFinished()) {
                $finalRestaurant = $this->showRestaurantFinal($session);
                $sessions[$key]->restaurant = $finalRestaurant[0];
            }
        }
        return $this->render('session/index.html.twig', [
            'sessions' => $sessions,
            'columns' => $this->columns,
        ]);
    }

    /**
     * @Route("/datatables", name="session_index_ajax", methods={"POST"})
     */
    public function indexAjax(XSessionRepository $sessionRepository, Request $request): JsonResponse
    {
        $sessionRepository->select(
            $request->request->get('start'),
            $request->request->get('length'),
            $request->request->get('order')[0]['column'],
            $request->request->get('order')[0]['dir'],
            $this->columns,
            $this->getUser(),
            $request->request->get('search')['value']
        );

        $data = $sessionRepository->arrayResults();

        foreach ($data as $key => $row) {
            $sessionObject = $this->getDoctrine()->getRepository(XSession::class)->find($row['id']);

            $data[$key]["actions"]["show"] = $this->generateUrl("session-id", ["sessionId" => $sessionObject->getSessionId()]);
            if ($this->getUser() == $sessionObject->getCreatorUserId()) {
                $data[$key]["actions"]["delete"] = $this->generateUrl("session_delete", ["id" => $row["id"]]);
                $data[$key]["actions"]["crsf"] = $this->tokenManager->getToken('delete' . $row['id'])->getValue();
            }
            $data[$key]["dateCreated"] = $row['dateCreated']->format("d. M Y H:i:s");
            if ($this->getDoctrine()->getRepository(UserList::class)->isEveryoneFinished($sessionObject)->getIsFinished()) {
                $finalRestaurant = $this->showRestaurantFinal($sessionObject);
                $data[$key]["finalRestaurant"]["name"] = $finalRestaurant[0]['name'];
                $data[$key]["finalRestaurant"]["link"] = $this->generateUrl('session_restaurant_show', ["id" => $row["id"]]);
            } else {
                $data[$key]["finalRestaurant"] = "None found yet everyone have to save.";
            }
        }

        $json = new \stdClass();
        $json->draw = $request->request->get('draw');
        $json->recordsTotal = $sessionRepository->findTotalSessions($this->getUser());
        $json->recordsFiltered = $sessionRepository->totalFiltered();
        $json->data = $data;

        return new JsonResponse($json, 200);
    }

    /**
     * @Route("/new", name="session-new")
     */
    public function new(Request $request): Response
    {
        $session = new XSession();
        $form = $this->createForm(XSessionType::class, $session);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $sessionId = uniqid();
            $session->setSessionId($sessionId);
            $session->setCreatorUserId($this->getUser());

            $entityManager->persist($session);

            $entityManager->flush();

            return $this->redirectToRoute('session-id', ['sessionId' => $sessionId]);
        }

        return $this->render('session/new.html.twig', [
            'restaurant' => $session,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/restaurant/{id}", name="session_restaurant_show", methods={"GET"})
     */
    public function showRestaurant(Restaurant $restaurant): Response
    {
        return $this->render('session/userShow.html.twig', [
            'restaurant' => $restaurant,
        ]);
    }

    /**
     * @Route("/{sessionId}", name="session-id")
     */
    public function session(XSession $session, Request $request): Response
    {
        $userList = $this->getDoctrine()->getRepository(UserList::class)->findOneBy([
            'xSessionId' => $session,
            'userId' => $this->getUser(),
        ]);

        if (!$userList) {
            $userList = $this->setUserList($session);
        }

        if ($request->isXmlHttpRequest() || $request->query->get('showJson') == 1) {

            if ($request->get('search')) {
                $jsonData = $this->search($request);

            } elseif ($request->get('addRestaurant') && !$userList->getIsFinished()) {
                $this->addRestaurant($request, $session);
                $jsonData['addedRestaurants'] = $this->showRestaurants($session);

            } elseif ($request->get('rateId') && !$userList->getIsFinished()) {
                $this->addRank($request);
                $jsonData = [];

            } elseif ($request->get('deleteRatedRestaurant') && !$userList->getIsFinished()) {
                $this->deleteRestaurant($request);
                $jsonData['addedRestaurants'] = $this->showRestaurants($session);

            } elseif ($request->get('restaurantFinalList')) {
                $jsonData['restaurantFinalList'] = $this->showRestaurantFinal($session);

            } elseif ($request->get('isFinished') && !$userList->getIsFinished()) {
                $this->setFinished($session, $this->getUser(), true);
                $jsonData = [];

            } elseif ($request->get('isUnFinished')) {
                $this->setFinished($session, $this->getUser(), false);
                $jsonData = [];
            }
            return new JsonResponse($jsonData);
        } else {

            return $this->render('session/session.html.twig', [
                'session' => $session,
                'myRestaurants' => $this->showRestaurants($session),
                'finalRestaurants' => $this->showRestaurantFinal($session),
                'userList' => $userList,
            ]);
        }
    }

    /**
     * @Route("/delete/{id}", name="session_delete", methods={"DELETE"})
     */
    public function delete(Request $request, XSession $session): Response
    {
        if ($this->isCsrfTokenValid('delete' . $session->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($session);
            $entityManager->flush();
        }

        return $this->redirectToRoute('session');
    }

    private function search(Request $request): ?array
    {
        $jsonData = [];
        $restaurants = $this->getDoctrine()->getRepository(Restaurant::class)->searchByStr($request->get('search'));

        foreach ($restaurants as $restaurant) {
            /**
             * @var $restaurant Restaurant
             */

            $jsonData[] = [
                'name' => $restaurant->getName(),
                'id' => $restaurant->getId(),
            ];
        }
        return ['search' => $jsonData];
    }

    private function addRestaurant(Request $request, xSession $session)
    {
        $em = $this->getDoctrine()->getManager();
        $restaurant = $this->getDoctrine()->getRepository(Restaurant::class)->find($request->get('addRestaurant'));
        $xSessionRank = $this->getDoctrine()->getRepository(xSessionRank::class)->findBy([
            'userId' => $this->getUser(),
            'xSession' => $session,
            'restaurantId' => $restaurant,
        ]);
        $userList = $this->getDoctrine()->getRepository(UserList::class)->findBy([
            'userId' => $this->getUser(),
            'xSessionId' => $session,
        ]);

        if (!$userList) {
            $this->setUserList($session);
        }

        if (!$xSessionRank) {
            $xSessionRank = new xSessionRank();
            $xSessionRank
                ->setUserId($this->getUser())
                ->setRank($this->getParameter('defaultRank'))
                ->setRestaurantId($restaurant)
                ->setXSession($session);

            $em->persist($xSessionRank);
            $em->flush();
            $em->refresh($session);
        }
    }

    private function showRestaurants(xSession $session): ?array
    {
        $jsonData = [];
        foreach ($this->getDoctrine()->getRepository(XSessionRank::class)->findBy([
            'xSession' => $session,
            'userId' => $this->getUser()
        ]) as $xSessionRank) {
            /**
             * @var $restaurant Restaurant
             * @var $xSessionRank xSessionRank
             */

            $restaurant = $xSessionRank->getRestaurantId();

            $jsonData[] = [
                'name' => $restaurant->getName(),
                'id' => $xSessionRank->getId(),
                'rank' => $xSessionRank->getRank(),
                'link' => $this->generateUrl("session_restaurant_show", ["id" => $xSessionRank->getRestaurantId()->getId()]),
                'image' => Image::open($restaurant->getImage()->getPathname())->resize(100)->guess(),
            ];
        }
        return $jsonData;
    }

    private function addRank(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $xSessionRank = $this->getDoctrine()->getRepository(xSessionRank::class)->findOneBy([
            'id' => $request->get('rateId'),
            'userId' => $this->getUser(),
        ]);
        $xSessionRank->setRank($request->get('value') * $this->getParameter('starMultiplier'));

        $em->persist($xSessionRank);
        $em->flush();
    }

    private function deleteRestaurant(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $xSessionRank = $this->getDoctrine()->getRepository(xSessionRank::class)->findOneBy([
            'id' => $request->get('deleteRatedRestaurant'),
            'userId' => $this->getUser(),
        ]);

        $em->remove($xSessionRank);
        $em->flush();
    }

    private function showRestaurantFinal(XSession $session): ?array
    {
        $jsonData = [];
        $finalXSessionRanksData = $this->getDoctrine()->getRepository(XSessionRank::class)->findFinalRestaurants($session);

        foreach ($finalXSessionRanksData as $key => $xSessionRankData) {
            /**
             * @var $restaurant Restaurant
             * @var $xSessionRank xSessionRank
             */
            $xSessionRank = $xSessionRankData['xSessionRank'];
            $restaurant = $xSessionRank->getRestaurantId();
//            $userList = $this->getDoctrine()->getRepository(UserList::class)->findOneBy([
//                'xSessionId' => $session,
//                'userId' => $xSessionRank->getUserId(),
//            ]);

            $jsonData[] = [
                'name' => $restaurant->getName(),
                'id' => $xSessionRank->getId(),
                'rank' => $xSessionRankData['rank'] / $this->getParameter('starMultiplier'),
                'votes' => $xSessionRankData['votes'],
                'link' => $this->generateUrl("session_restaurant_show", ["id" => $xSessionRank->getRestaurantId()->getId()]),
                'image' => Image::open($restaurant->getImage()->getPathname())->resize(100)->guess(),
                'isFinished' => $this->getDoctrine()->getRepository(UserList::class)->isEveryoneFinished($session)->getIsFinished() && $key == 0,
            ];
        }

        return $jsonData;
    }

    private function setFinished(XSession $session, User $user, bool $finished)
    {
        $em = $this->getDoctrine()->getManager();
        $userList = $this->getDoctrine()->getRepository(UserList::class)->findOneBy([
            'xSessionId' => $session,
            'userId' => $user,
        ]);
        $userList->setIsFinished($finished);

        $em->persist($userList);
        $em->flush();
    }

    private function setUserList(XSession $session): UserList
    {
        $em = $this->getDoctrine()->getManager();
        $userList = new UserList();
        $userList
            ->setUserId($this->getUser())
            ->setXSessionId($session)
            ->setIsFinished(false);

        $em->persist($userList);
        $em->flush();
        return $userList;
    }
}
