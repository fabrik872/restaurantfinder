<?php


namespace App\Util;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImageTools
{
    private $file;
    private $saveLocation;

    public function __construct()
    {
        global $kernel;
        $this->saveLocation = $kernel->getContainer()->getParameter('fileUpload');
    }

    public function setFile(UploadedFile $uploadedFile): self
    {
        $file = new File($uploadedFile);
        $this->file = $file->getPathname();
//        dump($this->file);
        return $this;
    }

    public function getFileData(): ?File
    {
        if ($this->file) {
            if (!file_exists($this->file)) {
                $this->reinitialize();
            }
            return new File($this->file);
        } else {
            return null;
        }
    }

    public function save(): self
    {
        $file = $this->getFileData()->move($this->saveLocation, uniqid() . '.' . $this->getFileData()->guessExtension());
        $this->file = $file->getPathname();

        return $this;
    }

    public function delete(): bool
    {

        return unlink($this->getFileData()->getPathname());
    }

    public function reinitialize()
    {
        global $kernel;
        $filename = ltrim($this->file, $this->saveLocation);
        $this->saveLocation = $kernel->getContainer()->getParameter('fileUpload');
        $this->file = $this->saveLocation . '/' . $filename;

    }

}