<?php

namespace App\Repository;

use App\Entity\Restaurant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Gregwar\Image\Image;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Restaurant|null find($id, $lockMode = null, $lockVersion = null)
 * @method Restaurant|null findOneBy(array $criteria, array $orderBy = null)
 * @method Restaurant[]    findAll()
 * @method Restaurant[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RestaurantRepository extends ServiceEntityRepository
{
    private $totalFiltered;
    private $arrayResults;

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Restaurant::class);
    }

    /**
     * @return Restaurant[] Returns an array of Restaurant objects
     */
    public function searchByStr(string $str)
    {

        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT r 
            FROM App\Entity\Restaurant r 
            WHERE r.name LIKE :str
            OR r.keywords LIKE :str
            OR r.description LIKE :str'
        );
        $query->setParameters(['str' => '%' . $str . '%']);

        return $query->getResult();
    }

    public function select(int $start, int $lenght, string $column, string $type, array $columns, string $search = null)
    {
        $entityManager = $this->getEntityManager();

        $search = "%" . $search . "%";

        $query = $entityManager->createQuery(
            'SELECT count(r) AS totalFiltered
            FROM App\Entity\Restaurant r 
            WHERE r.name LIKE :search
            OR r.keywords LIKE :search
            OR r.description LIKE :search
        ')
            ->setParameters([
                "search" => $search,
            ]);
        $this->totalFiltered = $query->getOneOrNullResult()["totalFiltered"];

        $select = '';
        foreach ($columns as $row) {
            $select .= 'r.' . $row;
            $select .= next($columns) ? ", " : "";
        }

        $query = $entityManager->createQuery(
            'SELECT ' . $select . '
            FROM App\Entity\Restaurant r 
            WHERE r.name LIKE :search
            OR r.keywords LIKE :search
            OR r.description LIKE :search
            ORDER BY r.' . $column . ' ' . $type . '
        ')
            ->setMaxResults($lenght)
            ->setFirstResult($start)
            ->setParameters([
                "search" => $search,
            ]);
        $this->arrayResults = $query->getArrayResult();
    }

    public function findTotalRestaurants(): ?int
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT COUNT(r) AS Restaurants FROM App\Entity\Restaurant r'
        );

        $query->execute();
        return $query->getOneOrNullResult()['Restaurants'];
    }

    public function totalFiltered(): int
    {
        return $this->totalFiltered;
    }

    public function arrayResults(): array
    {
        return $this->arrayResults;
    }

    // /**
    //  * @return Restaurant[] Returns an array of Restaurant objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Restaurant
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
