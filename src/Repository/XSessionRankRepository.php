<?php

namespace App\Repository;

use App\Entity\User;
use App\Entity\XSession;
use App\Entity\XSessionRank;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method XSessionRank|null find($id, $lockMode = null, $lockVersion = null)
 * @method XSessionRank|null findOneBy(array $criteria, array $orderBy = null)
 * @method XSessionRank[]    findAll()
 * @method XSessionRank[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class XSessionRankRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, XSessionRank::class);
    }

    /**
     * @return XSessionRank[] Returns an array of XSessionRank objects
     */
    public function findFinalRestaurants(XSession $session)
    {

        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT xr AS xSessionRank, avg(xr.rank) AS rank, COUNT(xr) as votes
            FROM App\Entity\XSessionRank xr
            WHERE xr.xSession = :xSessionId
            GROUP BY xr.restaurantId
            ORDER BY rank DESC
            '
        );
        $query->setParameters([
            'xSessionId' => $session,
        ]);

        // returns an array of Product objects
        return $query->getResult();
    }

    // /**
    //  * @return XSessionRank[] Returns an array of XSessionRank objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('x')
            ->andWhere('x.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('x.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?XSessionRank
    {
        return $this->createQueryBuilder('x')
            ->andWhere('x.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
