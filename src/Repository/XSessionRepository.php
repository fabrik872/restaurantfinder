<?php

namespace App\Repository;

use App\Entity\User;
use App\Entity\XSession;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method XSession|null find($id, $lockMode = null, $lockVersion = null)
 * @method XSession|null findOneBy(array $criteria, array $orderBy = null)
 * @method XSession[]    findAll()
 * @method XSession[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class XSessionRepository extends ServiceEntityRepository
{
    private $totalFiltered;
    private $arrayResults;

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, XSession::class);
    }

    /**
     * @return int
     */
    public function dailyRestaurants(): ?int
    {

        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT COUNT(s) AS dailyRestaurants
            FROM App\Entity\XSession s
            WHERE EXISTS (SELECT ul FROM App\Entity\UserList ul WHERE ul.xSessionId = s.id AND ul.isFinished = true ORDER BY ul.isFinished ASC)
            AND s.createDate >= :yesterday
            '
        );
        $query->setParameters([
            'yesterday' => new \DateTime('-24 hours')
        ]);

        return $query->getOneOrNullResult()['dailyRestaurants'];
    }

    /**
     * @return XSession[]
     */
    public function findSessionWithUser(User $user): ?iterable
    {

        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT s
            FROM App\Entity\XSession s
            JOIN App\Entity\UserList ul
            WHERE ul.xSessionId = s.id 
            AND ul.userId = :user
            '
        );
        $query->setParameters([
            'user' => $user,
        ]);

        return $query->getResult();
    }

    public function select(int $start, int $lenght, int $column, string $type, array $columns, User $user, string $search = null)
    {
        $entityManager = $this->getEntityManager();

        $search = "%" . $search . "%";

        $query = $entityManager->createQuery(
            'SELECT count(s) AS totalFiltered
            FROM App\Entity\XSession s
            JOIN App\Entity\UserList ul
            WHERE ul.xSessionId = s.id 
            AND ul.userId = :user
            AND s.description LIKE :search
        ')
            ->setParameters([
                "search" => $search,
                'user' => $user,
            ]);
        $this->totalFiltered = $query->getOneOrNullResult()["totalFiltered"];

        $order = [
            0 => "s.id",
            1 => "u.email",
            3 => "s.description",
            4 => "s.createDate"
        ];

        $query = $entityManager->createQuery(
            'SELECT 
                s.id as ' . $columns[0] . ', 
                u.email as ' . $columns[1] . ', 
                s.description as ' . $columns[3] . ', 
                s.createDate as ' . $columns[4] . ' 
            FROM App\Entity\XSession s
            JOIN App\Entity\UserList ul
            JOIN App\Entity\User u
            WHERE ul.xSessionId = s.id
            AND u.id = s.creatorUserId 
            AND ul.userId = :user
            AND s.description LIKE :search
            ORDER BY ' . $order[$column] . ' ' . $type . '
        ')
            ->setMaxResults($lenght)
            ->setFirstResult($start)
            ->setParameters([
                "search" => $search,
                'user' => $user,
            ]);
        $this->arrayResults = $query->getArrayResult();
    }

    public function findTotalSessions(User $user)
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT count(s) as sessions
            FROM App\Entity\XSession s
            JOIN App\Entity\UserList ul
            WHERE ul.xSessionId = s.id 
            AND ul.userId = :user'
        );


        $query->setParameters([
            'user' => $user,
        ]);
        return $query->getOneOrNullResult()['sessions'];
    }

    public function totalFiltered(): int
    {
        return $this->totalFiltered;
    }

    public function arrayResults(): array
    {
        return $this->arrayResults;
    }

    // /**
    //  * @return XSession[] Returns an array of XSession objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('x')
            ->andWhere('x.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('x.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?XSession
    {
        return $this->createQueryBuilder('x')
            ->andWhere('x.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
