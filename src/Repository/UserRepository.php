<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    private $totalFiltered;
    private $arrayResults;

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function select(int $start, int $lenght, string $column, string $type, array $columns, string $search = null)
    {
        $entityManager = $this->getEntityManager();

        $search = "%" . $search . "%";

        $query = $entityManager->createQuery(
            'SELECT count(u) AS totalFiltered
            FROM App\Entity\User u 
            WHERE u.email LIKE :search
            OR u.createDate LIKE :search
        ')
            ->setParameters([
                "search" => $search,
            ]);
        $this->totalFiltered = $query->getOneOrNullResult()["totalFiltered"];

        $select = '';
        foreach ($columns as $row) {
            $select .= 'u.' . $row;
            $select .= next($columns) ? ", " : "";
        }

        $query = $entityManager->createQuery(
            'SELECT ' . $select . '
            FROM App\Entity\User u 
            WHERE u.email LIKE :search
            OR u.createDate LIKE :search
            ORDER BY u.' . $column . ' ' . $type . '
        ')
            ->setMaxResults($lenght)
            ->setFirstResult($start)
            ->setParameters([
                "search" => $search,
            ]);
        $this->arrayResults = $query->getArrayResult();
    }

    public function findTotalUsers()
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT COUNT(u) AS Users FROM App\Entity\User u'
        );

        $query->execute();
        return $query->getOneOrNullResult()['Users'];
    }

    public function totalFiltered(): int
    {
        return $this->totalFiltered;
    }

    public function arrayResults(): array
    {
        return $this->arrayResults;
    }

    // /**
    //  * @return User[] Returns an array of User objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
