<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\XSessionRankRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE")
 */
class XSessionRank
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="xSessionRanks")
     */
    private $userId;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Restaurant", inversedBy="xSessionRanks")
     */
    private $restaurantId;

    /**
     * @ORM\Column(type="integer")
     */
    private $rank;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\XSession", inversedBy="xSessionRankId")
     */
    private $xSession;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserId(): ?User
    {
        return $this->userId;
    }

    public function setUserId(?User $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    public function getRestaurantId(): ?Restaurant
    {
        return $this->restaurantId;
    }

    public function setRestaurantId(?Restaurant $restaurantId): self
    {
        $this->restaurantId = $restaurantId;

        return $this;
    }

    public function getRank(): ?int
    {
        return $this->rank;
    }

    public function setRank(int $rank): self
    {
        $this->rank = $rank;

        return $this;
    }

    public function getXSession(): ?XSession
    {
        return $this->xSession;
    }

    public function setXSession(?XSession $xSession): self
    {
        $this->xSession = $xSession;

        return $this;
    }
}
