<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\XSessionRepository")
 * @ORM\Table(name="XSession",indexes={@ORM\Index(name="sessionId_idx", columns={"session_id"})})
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE")
 */
class XSession
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\xSessionRank", mappedBy="xSession", cascade={"persist", "remove"})
     */
    private $xSessionRankId;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="xSessions")
     */
    private $creatorUserId;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $sessionId;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserList", mappedBy="xSessionId", cascade={"persist", "remove"})
     */
    private $userLists;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createDate;

    public function __construct()
    {
        $this->xSessionRankId = new ArrayCollection();
        $this->userLists = new ArrayCollection();
        $this->createDate = new \DateTime('now');
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|xSessionRank[]
     */
    public function getXSessionRankId(): Collection
    {
        return $this->xSessionRankId;
    }

    public function addXSessionRankId(xSessionRank $xSessionRankId): self
    {
        if (!$this->xSessionRankId->contains($xSessionRankId)) {
            $this->xSessionRankId[] = $xSessionRankId;
            $xSessionRankId->setXSession($this);
        }

        return $this;
    }

    public function removeXSessionRankId(xSessionRank $xSessionRankId): self
    {
        if ($this->xSessionRankId->contains($xSessionRankId)) {
            $this->xSessionRankId->removeElement($xSessionRankId);
            // set the owning side to null (unless already changed)
            if ($xSessionRankId->getXSession() === $this) {
                $xSessionRankId->setXSession(null);
            }
        }

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCreatorUserId(): ?User
    {
        return $this->creatorUserId;
    }

    public function setCreatorUserId(?User $creatorUserId): self
    {
        $this->creatorUserId = $creatorUserId;

        return $this;
    }

    public function getSessionId(): ?string
    {
        return $this->sessionId;
    }

    public function setSessionId(string $sessionId): self
    {
        $this->sessionId = $sessionId;

        return $this;
    }

    /**
     * @return Collection|UserList[]
     */
    public function getUserLists(): Collection
    {
        return $this->userLists;
    }

    public function addUserList(UserList $userList): self
    {
        if (!$this->userLists->contains($userList)) {
            $this->userLists[] = $userList;
            $userList->setXSessionId($this);
        }

        return $this;
    }

    public function removeUserList(UserList $userList): self
    {
        if ($this->userLists->contains($userList)) {
            $this->userLists->removeElement($userList);
            // set the owning side to null (unless already changed)
            if ($userList->getXSessionId() === $this) {
                $userList->setXSessionId(null);
            }
        }

        return $this;
    }

    public function getCreateDate(): ?\DateTimeInterface
    {
        return $this->createDate;
    }

    public function setCreateDate(\DateTimeInterface $createDate): self
    {
        $this->createDate = $createDate;

        return $this;
    }
}
