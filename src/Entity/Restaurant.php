<?php

namespace App\Entity;

use App\Util\ImageTools;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RestaurantRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE")
 */
class Restaurant
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="text")
     * @var $image ImageTools
     */
    private $image;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\XSessionRank", mappedBy="restaurantId", cascade={"persist", "remove"})
     */
    private $xSessionRanks;

    /**
     * @ORM\Column(type="text")
     */
    private $keywords;

    public function __construct()
    {
        $this->xSessionRanks = new ArrayCollection();
        $this->image = $this->serialize(new ImageTools());
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getImage()
    {
        /**
         * @var $imageTools ImageTools
         */
        $imageTools = $this->deserialize($this->image);

        return $imageTools->getFileData();
    }

    public function setImage(UploadedFile $uploadedFile): self
    {
        /**
         * @var $imageTools ImageTools
         */
        $imageTools = $this->deserialize($this->image);
        $imageTools->setFile($uploadedFile);
        $imageTools->save();
        $this->image = $this->serialize($imageTools);

        return $this;
    }

    /**
     * @return Collection|XSessionRank[]
     */
    public function getXSessionRanks(): Collection
    {
        return $this->xSessionRanks;
    }

    public function addXSessionRank(XSessionRank $xSessionRank): self
    {
        if (!$this->xSessionRanks->contains($xSessionRank)) {
            $this->xSessionRanks[] = $xSessionRank;
            $xSessionRank->setRestaurantId($this);
        }

        return $this;
    }

    public function removeXSessionRank(XSessionRank $xSessionRank): self
    {
        if ($this->xSessionRanks->contains($xSessionRank)) {
            $this->xSessionRanks->removeElement($xSessionRank);
            // set the owning side to null (unless already changed)
            if ($xSessionRank->getRestaurantId() === $this) {
                $xSessionRank->setRestaurantId(null);
            }
        }

        return $this;
    }

    private function serialize($object): string
    {
        return serialize($object);
    }

    private function deserialize(string $string)
    {
        return unserialize($string);
    }

    public function getKeywords(): ?string
    {
        return $this->keywords;
    }

    public function setKeywords(string $keywords): self
    {
        $this->keywords = $keywords;

        return $this;
    }
}
