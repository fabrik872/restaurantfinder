<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createDate;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\XSessionRank", mappedBy="userId", cascade={"persist", "remove"})
     */
    private $xSessionRanks;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\XSession", mappedBy="creatorUserId", cascade={"persist", "remove"})
     */
    private $xSessions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserList", mappedBy="userId", cascade={"persist", "remove"})
     */
    private $userLists;

    public function __construct()
    {
        $this->createDate = new \DateTime('now');
        $this->xSessionRanks = new ArrayCollection();
        $this->xSessions = new ArrayCollection();
        $this->userLists = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getCreateDate(): ?\DateTimeInterface
    {
        return $this->createDate;
    }

    public function setCreateDate(\DateTimeInterface $createDate): self
    {
        $this->createDate = $createDate;

        return $this;
    }

    /**
     * @return Collection|XSessionRank[]
     */
    public function getXSessionRanks(): Collection
    {
        return $this->xSessionRanks;
    }

    public function addXSessionRank(XSessionRank $xSessionRank): self
    {
        if (!$this->xSessionRanks->contains($xSessionRank)) {
            $this->xSessionRanks[] = $xSessionRank;
            $xSessionRank->setUserId($this);
        }

        return $this;
    }

    public function removeXSessionRank(XSessionRank $xSessionRank): self
    {
        if ($this->xSessionRanks->contains($xSessionRank)) {
            $this->xSessionRanks->removeElement($xSessionRank);
            // set the owning side to null (unless already changed)
            if ($xSessionRank->getUserId() === $this) {
                $xSessionRank->setUserId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|XSession[]
     */
    public function getXSessions(): Collection
    {
        return $this->xSessions;
    }

    public function addXSession(XSession $xSession): self
    {
        if (!$this->xSessions->contains($xSession)) {
            $this->xSessions[] = $xSession;
            $xSession->setCreatorUserId($this);
        }

        return $this;
    }

    public function removeXSession(XSession $xSession): self
    {
        if ($this->xSessions->contains($xSession)) {
            $this->xSessions->removeElement($xSession);
            // set the owning side to null (unless already changed)
            if ($xSession->getCreatorUserId() === $this) {
                $xSession->setCreatorUserId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserList[]
     */
    public function getUserLists(): Collection
    {
        return $this->userLists;
    }

    public function addUserList(UserList $userList): self
    {
        if (!$this->userLists->contains($userList)) {
            $this->userLists[] = $userList;
            $userList->setUserId($this);
        }

        return $this;
    }

    public function removeUserList(UserList $userList): self
    {
        if ($this->userLists->contains($userList)) {
            $this->userLists->removeElement($userList);
            // set the owning side to null (unless already changed)
            if ($userList->getUserId() === $this) {
                $userList->setUserId(null);
            }
        }

        return $this;
    }
}
