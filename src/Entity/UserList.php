<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserListRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE")
 */
class UserList
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="userLists")
     */
    private $userId;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\XSession", inversedBy="userLists")
     */
    private $xSessionId;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isFinished;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserId(): ?User
    {
        return $this->userId;
    }

    public function setUserId(?User $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    public function getXSessionId(): ?XSession
    {
        return $this->xSessionId;
    }

    public function setXSessionId(?XSession $xSessionId): self
    {
        $this->xSessionId = $xSessionId;

        return $this;
    }

    public function getIsFinished(): ?bool
    {
        return $this->isFinished;
    }

    public function setIsFinished(bool $isFinished): self
    {
        $this->isFinished = $isFinished;

        return $this;
    }
}
