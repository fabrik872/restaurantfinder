## Installation:


- Make shure you have installed PHP, Composer and Docker
- Clone project from repository with:
```
$ git clone https://fabrik872@bitbucket.org/fabrik872/restaurantfinder.git
```
- Move into project folder with:
```
$ cd restaurantfinder
```
- Execute $ composer install
- Execute $ ./install.sh
- Go to [http://localhost:81](http://localhost:81)