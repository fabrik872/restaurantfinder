#!/usr/bin/env bash

echo "Updating user with id=1 as admin"

sudo docker container exec -d restaurantfinder_web_1 bin/console /var/www/bin/console doctrine:schema:update -f

sudo docker container exec -d restaurantfinder_web_1 bin/console doctrine:query:dql "UPDATE App\Entity\User u SET u.roles = '[\"ROLE_ADMIN\"]' WHERE u.id = 1"